#include <stdio.h>
#include <stdlib.h>

#include "simplog_global.h"

void
onEvent(const Simplog_Event* pEvent, const char* message, void* pUserData) {
  printf("----- onEvent -----\n"
         "%s\n"
         "File: %s\n"
         "Function: %s\n"
         "Line: %d\n"
         "message: '%s'\n"
         "---------------\n",
         Simplog_strLevelFullName(pEvent->level),
         pEvent->file,
         pEvent->function,
         pEvent->line,
         message);
}

int
main(void) {
  Simplog_Allocator alloc = {
    .malloc = malloc,
    .free = free,
  };
  Simplog_Config config;
  config.eventCb = onEvent;
  config.flushCb = NULL;
  config.pAllocator = &alloc;
  config.level = SIMPLOG_LEVEL_VERBOSE;

  LOG_CREATE(config, NULL);

  LOG_V("test1");
  LOG_D("test2");
  LOG_I("test3");
  LOG_W("test4");
  LOG_E("test5");

  LOG_DESTROY();

  return 0;
}