#include <assert.h>
#include <string.h>

#include "simplog.h"

#define STR_EQUAL(a, b) assert(0 == strcmp(a, b))

int
main(void) {
  STR_EQUAL("V", Simplog_strLevelShortName(SIMPLOG_LEVEL_VERBOSE));
  STR_EQUAL("D", Simplog_strLevelShortName(SIMPLOG_LEVEL_DEBUG));
  STR_EQUAL("I", Simplog_strLevelShortName(SIMPLOG_LEVEL_INFO));
  STR_EQUAL("W", Simplog_strLevelShortName(SIMPLOG_LEVEL_WARNING));
  STR_EQUAL("E", Simplog_strLevelShortName(SIMPLOG_LEVEL_ERROR));

  STR_EQUAL("VERBOSE", Simplog_strLevelFullName(SIMPLOG_LEVEL_VERBOSE));
  STR_EQUAL("DEBUG", Simplog_strLevelFullName(SIMPLOG_LEVEL_DEBUG));
  STR_EQUAL("INFO", Simplog_strLevelFullName(SIMPLOG_LEVEL_INFO));
  STR_EQUAL("WARNING", Simplog_strLevelFullName(SIMPLOG_LEVEL_WARNING));
  STR_EQUAL("ERROR", Simplog_strLevelFullName(SIMPLOG_LEVEL_ERROR));

  return 0;
}