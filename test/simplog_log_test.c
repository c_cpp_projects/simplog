#include <stdio.h>

#include "simplog.h"
#include "simplog_macros.h"

void
onEvent(const Simplog_Event* pEvent, const char* message, void* pUserData) {

  char timestr[20];
#if (0 != SIMPLOG_ADD_TIMESTAMPS)
  strftime(timestr, sizeof(timestr), "%d.%m.%Y %T", localtime(&pEvent->timestamp.tv_sec));
  const long millis = pEvent->timestamp.tv_nsec / 1000000;
#else
  timestr[0] = '0';
  timestr[1] = '\0';
  const long millis = 0;
#endif

  printf("----- onEvent -----\n"
         "%s\n"
         "%s.%03ld\n"
         "File: %s\n"
         "Function: %s\n"
         "Line: %d\n"
         "message: '%s'\n"
         "---------------",
         Simplog_strLevelFullName(pEvent->level),
         timestr,
         millis,
         pEvent->file,
         pEvent->function,
         pEvent->line,
         message);
}

int
main(void) {
  Simplog_Config config;
  config.eventCb = onEvent;
  config.flushCb = NULL;
  config.pAllocator = NULL;
  config.level = SIMPLOG_LEVEL_VERBOSE;

  Simplog simp = Simplog_create(config, NULL);
  SIMPLOG_I(simp, "nona %d", 2);

  Simplog_destroy(simp);

  return 0;
}
