## 1.1.4

  *  Changed compile options

## 1.1.3

  * Added compile definitions for project version.

## 1.1.2

  * Changed compiler configurations.
  
# 1.1.1

  * Added .clang-format .

## 1.1.0

  * Fixed a bug in `Simplog_log` with how the va_list was handled. Previously there were cases where the arguments were processed incorrectly which resulted in incorrect log messages.