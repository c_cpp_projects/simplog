#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <string.h>

#include "simplog.h"



#define CHECK_NULL(x) \
  if (NULL == (x))    \
    return;

/******************** Definition of Private Structs ********************/

struct Simplog {
  Simplog_Config config;
  void* pUserData;
};

/******************** Declaration of Private Functions ********************/

static inline bool
Simplog_isValidAllocator(Simplog_Allocator* pAllocator);

/******************** Definition of Public Functions ********************/

Simplog
Simplog_create(Simplog_Config config, void* pUserData) {
  Simplog simplog = NULL;

  if (NULL == config.pAllocator) {
    simplog = (Simplog) calloc(1, sizeof(struct Simplog));
  } else {
    if (!Simplog_isValidAllocator(config.pAllocator))
      return NULL;

    simplog = (Simplog) config.pAllocator->malloc(sizeof(struct Simplog));
    memset(simplog, 0, sizeof(struct Simplog));
  }

  simplog->config = config;
  simplog->pUserData = pUserData;

  return simplog;
}

void
Simplog_destroy(Simplog simplog) {
  CHECK_NULL(simplog)

  if (NULL == simplog->config.pAllocator) {
    free(simplog);
  } else {
    simplog->config.pAllocator->free(simplog);
  }
}

void
Simplog_flush(Simplog simplog) {
  CHECK_NULL(simplog)
  CHECK_NULL(simplog->config.flushCb)
  simplog->config.flushCb();
}

void
Simplog_log(Simplog simplog, const char* file, const char* function, unsigned int line,
            Simplog_Level level, const char* format, ...) {
  CHECK_NULL(simplog->config.eventCb)

#if (0 != SIMPLOG_ADD_TIMESTAMPS)
  struct timespec timestamp;
  timespec_get(&timestamp, TIME_UTC);
#endif

  if (NULL == simplog->config.pAllocator) {
    Simplog_Event event;
    event.file = file;
    event.function = function;
    event.line = line;
    event.level = level;
#if (0 != SIMPLOG_ADD_TIMESTAMPS)
    event.timestamp = timestamp;
#endif

    va_list args1;
    va_start(args1, format);

    // Create copy of args1, because args1 becomes
    // invalid after the first call to vsnprintf.
    va_list args2;
    va_copy(args2, args1);

    // Get the size needed for message buffer.
    int size = vsnprintf(NULL, 0, format, args1) + 1;
    va_end(args1);

    // Write the actual format string to message buffer.
    char message[size];
    vsnprintf(message, (size_t) size, format, args2);
    va_end(args2);

    simplog->config.eventCb(&event, message, simplog->pUserData);
  } else {
    Simplog_Event* pEvent = NULL;
    pEvent = (Simplog_Event*) simplog->config.pAllocator->malloc(sizeof(Simplog_Event));
    if (NULL == pEvent)
      return;

    pEvent->file = file;
    pEvent->function = function;
    pEvent->line = line;
    pEvent->level = level;
#if (0 != SIMPLOG_ADD_TIMESTAMPS)
    pEvent->timestamp = timestamp;
#endif

    va_list args1;
    va_start(args1, format);

    // Create copy of args1, because args1 becomes
    // invalid after the first call to vsnprintf.
    va_list args2;
    va_copy(args2, args1);

    // Get the size needed for message buffer.
    int size = vsnprintf(NULL, 0, format, args1) + 1;
    va_end(args1);

    // Allocate memory for message. Return on failure.
    char* message = (char*) simplog->config.pAllocator->malloc((size_t) size);
    if (NULL == message) {
      simplog->config.pAllocator->free(pEvent);
      va_end(args2);
      return;
    }

    // Write the actual format string to message buffer.
    vsnprintf(message, (size_t) size, format, args2);
    va_end(args2);

    simplog->config.eventCb(pEvent, message, simplog->pUserData);

    simplog->config.pAllocator->free(pEvent);
    simplog->config.pAllocator->free(message);
  }
}

void
Simplog_setConfig(Simplog* pSimplog, Simplog_Config config) {
  CHECK_NULL(pSimplog)

  Simplog simplog = *pSimplog;
  CHECK_NULL(simplog)

  // No change in allocators
  if (simplog->config.pAllocator == config.pAllocator) {
    simplog->config = config;
    return;
  }

  // Only apply new config if the new allocator is valid.
  if (!Simplog_isValidAllocator(config.pAllocator))
    return;

  void* pUserData = simplog->pUserData;
  Simplog_destroy(simplog);
  *pSimplog = Simplog_create(config, pUserData);
}

void
Simplog_setUserData(Simplog simplog, void* pUserData) {
  CHECK_NULL(simplog);
  simplog->pUserData = pUserData;
}

#if (0 != SIMPLOG_INCLUDE_FULL_NAMES)
const char*
Simplog_strLevelFullName(Simplog_Level level) {
  static const char* const names[] = {"VERBOSE", "DEBUG", "INFO", "WARNING", "ERROR"};
  return names[level];
}
#endif

#if (0 != SIMPLOG_INCLUDE_SHORT_NAMES)
const char*
Simplog_strLevelShortName(Simplog_Level level) {
  static const char* const names[] = {"V", "D", "I", "W", "E"};
  return names[level];
}
#endif



/******************** Definition of Private Functions ********************/

static inline bool
Simplog_isValidAllocator(Simplog_Allocator* pAllocator) {
  return (NULL != pAllocator->malloc && NULL != pAllocator->free);
}
