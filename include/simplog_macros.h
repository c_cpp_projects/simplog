#ifndef SIMPLOG_MACROS_H
#define SIMPLOG_MACROS_H

#include "simplog.h"

#if (0 != SIMPLOG_MACROS_ENABLE)

#  define SIMPLOG_FLUSH(simplog) Simplog_flush(simplog)

#  define SIMPLOG_V(simplog, format, ...) \
    Simplog_log(simplog,                  \
                __FILE__,                 \
                __func__,                 \
                __LINE__,                 \
                SIMPLOG_LEVEL_VERBOSE,    \
                format,                   \
                ##__VA_ARGS__)

#  define SIMPLOG_D(simplog, format, ...) \
    Simplog_log(                          \
      simplog, __FILE__, __func__, __LINE__, SIMPLOG_LEVEL_DEBUG, format, ##__VA_ARGS__)

#  define SIMPLOG_I(simplog, format, ...) \
    Simplog_log(                          \
      simplog, __FILE__, __func__, __LINE__, SIMPLOG_LEVEL_INFO, format, ##__VA_ARGS__)

#  define SIMPLOG_W(simplog, format, ...) \
    Simplog_log(simplog,                  \
                __FILE__,                 \
                __func__,                 \
                __LINE__,                 \
                SIMPLOG_LEVEL_WARNING,    \
                format,                   \
                ##__VA_ARGS__)

#  define SIMPLOG_E(simplog, format, ...) \
    Simplog_log(                          \
      simplog, __FILE__, __func__, __LINE__, SIMPLOG_LEVEL_ERROR, format, ##__VA_ARGS__)

#else

#  define SIMPLOG_FLUSH(simplog)
#  define SIMPLOG_V(simplog, format, ...)
#  define SIMPLOG_D(simplog, format, ...)
#  define SIMPLOG_I(simplog, format, ...)
#  define SIMPLOG_W(simplog, format, ...)
#  define SIMPLOG_E(simplog, format, ...)

#endif  // (0 != SIMPLOG_MACROS_ENABLE)

#endif  // SIMPLOG_MACROS_H