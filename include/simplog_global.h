#ifndef SIMPLOG_GLOBAL_H
#define SIMPLOG_GLOBAL_H

#include "simplog.h"
#include "simplog_macros.h"

#if (0 != SIMPLOG_GLOBAL_LOG_ENABLE)

/**
 * @brief The @c Simplog instance which is used in the macros:
 *        @b LOG_FLUSH ,
 *        @b LOG_V ,
 *        @b LOG_D ,
 *        @b LOG_I ,
 *        @b LOG_W ,
 *        @b LOG_E
 */
Simplog Simplog_globalInstance = NULL;

#  define LOG_CREATE(config, pUserData)                           \
    do {                                                          \
      if (NULL != Simplog_globalInstance)                         \
        continue;                                                 \
      Simplog_globalInstance = Simplog_create(config, pUserData); \
    } while (0)

#  define LOG_DESTROY()                        \
    do {                                       \
      Simplog_destroy(Simplog_globalInstance); \
      Simplog_globalInstance = NULL;           \
    } while (0)

#  define LOG_FLUSH() SIMPLOG_FLUSH(Simplog_globalInstance)

#  define LOG_V(format, ...) SIMPLOG_V(Simplog_globalInstance, format, ##__VA_ARGS__)
#  define LOG_D(format, ...) SIMPLOG_D(Simplog_globalInstance, format, ##__VA_ARGS__)
#  define LOG_I(format, ...) SIMPLOG_I(Simplog_globalInstance, format, ##__VA_ARGS__)
#  define LOG_W(format, ...) SIMPLOG_W(Simplog_globalInstance, format, ##__VA_ARGS__)
#  define LOG_E(format, ...) SIMPLOG_E(Simplog_globalInstance, format, ##__VA_ARGS__)

#  define LOG_SET_CONFIG(config) Simplog_setConfig(Simplog_globalInstance, config)

#  define LOG_SET_USERDATA(pUserData) \
    Simplog_setUserData(Simplog_globalInstance, pUserData)

#else

#  define LOG_CREATE(config, pUserData)
#  define LOG_DESTROY()
#  define LOG_FLUSH()
#  define LOG_V(format, ...)
#  define LOG_D(format, ...)
#  define LOG_I(format, ...)
#  define LOG_W(format, ...)
#  define LOG_E(format, ...)
#  define LOG_SET_CONFIG(config)
#  define LOG_SET_USERDATA(pUserData)

#endif  // (0 != SIMPLOG_GLOBAL_LOG_ENABLE)

#endif  // SIMPLOG_GLOBAL_H