#ifndef SIMPLOG_H
#define SIMPLOG_H

#include <stddef.h>

#if (0 != SIMPLOG_ADD_TIMESTAMPS)
#  include <time.h>
#endif



typedef enum {
  SIMPLOG_LEVEL_VERBOSE,
  SIMPLOG_LEVEL_DEBUG,
  SIMPLOG_LEVEL_INFO,
  SIMPLOG_LEVEL_WARNING,
  SIMPLOG_LEVEL_ERROR,
} Simplog_Level;

typedef struct {
  const char* file;
  const char* function;
  unsigned int line;
  Simplog_Level level;
#if (0 != SIMPLOG_ADD_TIMESTAMPS)
  struct timespec timestamp;
#endif
} Simplog_Event;

typedef void (*Simplog_EventCallback)(const Simplog_Event* event, const char* message,
                                      void* pUserData);
typedef void (*Simplog_FlushCallback)(void);
typedef void* (*Simplog_malloc)(size_t size);
typedef void (*Simplog_free)(void* pointer);

typedef struct {
  Simplog_malloc malloc;
  Simplog_free free;
} Simplog_Allocator;

typedef struct {
  /**
   * The @c Simplog_EventCallback that handles all log events.
   */
  Simplog_EventCallback eventCb;

  /**
   * The @c Simplog_FlushCallback that flushes all queued events.
   */
  Simplog_FlushCallback flushCb;

  /**
   * The @c Simplog_Allocator used for allocating memory for the
   * log event and message. If @c pAllocator is set to @b NULL
   * then the memory is put on the stack.
   */
  Simplog_Allocator* pAllocator;

  Simplog_Level level;
} Simplog_Config;

typedef struct Simplog* Simplog;



/**
 * @brief Create a new @c Simplog structure.
 *
 * @return @b NULL on failure.
 */
Simplog
Simplog_create(Simplog_Config config, void* pUserData);

/**
 * @brief Destroy this @c Simplog structure.
 *        Do not use it afterwars, otherwise you will expirience
 *        undefined behaviour.
 */
void
Simplog_destroy(Simplog simplog);

/**
 * @brief Execute the registered @c Simplog_FlushCallback , if there is one.
 */
void
Simplog_flush(Simplog simplog);

/**
 * @brief Log an event.
 *
 * @param file The file in which the event occured.
 * @param function The function in which the event occured.
 * @param line The line at which the event occured.
 * @param level The severity of the event.
 * @param format A format string.
 */
void
Simplog_log(Simplog simplog, const char* file, const char* function, unsigned int line,
            Simplog_Level level, const char* format, ...);

/**
 * @brief Change the @c Simplog_Config of @c pSimplog .
 */
void
Simplog_setConfig(Simplog* pSimplog, Simplog_Config config);

void
Simplog_setUserData(Simplog simplog, void* pUserData);

#if (0 != SIMPLOG_INCLUDE_FULL_NAMES)
/**
 * @brief Get the full name of @c level .
 */
const char*
Simplog_strLevelFullName(Simplog_Level level);
#endif

#if (0 != SIMPLOG_INCLUDE_SHORT_NAMES)
/**
 * @brief Get the short name of @c level .
 */
const char*
Simplog_strLevelShortName(Simplog_Level level);
#endif



#endif  // SIMPLOG_H